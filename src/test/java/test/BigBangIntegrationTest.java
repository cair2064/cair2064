package test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BigBangIntegrationTest {
    private RepositoryContact repC;
    private RepositoryActivity repA;
    private static final String INVALID_NAME = "Invalid name";
    private static final String INVALID_PHONE_NUMBER = "Invalid phone number";

    @Before
    public void setUp(){
        repC = new RepositoryContactMock();
        repA = new RepositoryActivityMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    /*
    Test unitar pachet A - invalid ( phone length < 10 )
     */
    @Test
    public void testA() throws InvalidFormatException {
        expectedEx.expect(InvalidFormatException.class);
        expectedEx.expectMessage(INVALID_PHONE_NUMBER);
        Contact contact = new Contact("Cineva", "Str. Marin Preda, nr. 15", "071562548", "email@domain.com");
        repC.addContact(contact);
    }

    /*
    Test unitar pachet B - valid
     */
    @Test
    public void testB() throws  Exception{
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("name1",
                df.parse("01/01/2010 12:00"),
                df.parse("02/02/2010 12:00"),
                null,
                "Lunch break");

        repA.addActivity(activity);
        assertEquals(1, repA.getActivities().size());
    }

    /*
    Test unitar pachet C - invalid
    */
    @Test
    public void testInvalidC() throws Exception {

        List<Contact> contacts = new ArrayList<Contact>();
        Contact contact = new Contact("name2", "address1", "+40711223344", "email1@domain1.com");
        contacts.add(contact);

        for (Activity act : repA.getActivities())
            repA.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name1", start, end,
                contacts, "description2");

        repA.addActivity(act);

        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(contact.getName(), c.getTime());
        assertTrue(result.size() == 0);
    }

    /*
   Test unitar pachet C - valid
   */
    @Test
    public void testC() throws Exception {

        List<Contact> contacts = new ArrayList<Contact>();
        Contact contact = new Contact("name2", "address1", "+40711223344", "email1@domain1.com");
        contacts.add(contact);

        for (Activity act : repA.getActivities())
            repA.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name2", start, end,
                contacts, "description2");

        repA.addActivity(act);

        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(contact.getName(), c.getTime());
        assertTrue(result.size() == 1);
    }

    /*
    Test de integrare |P -> B -> C -> A| cu B-valid, C-valid, A-invalid
     */
    @Test
    public void testIntegrareABC() throws InvalidFormatException {
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(new Contact("Name1", "address1", "+40711223344","email1@domain1.com"));

        for (Activity act : repA.getActivities())
            repA.removeActivity(act);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("nume1", start, end,
                contacts, "description2");

        try {
            repA.addActivity(act);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //B
        assertTrue(1 == repA.count());
        //C
        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
        assertTrue(result.size() == 1);
        //A
        expectedEx.expect(InvalidFormatException.class);
        expectedEx.expectMessage(INVALID_NAME);
        Contact contact = new Contact("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "Str. Autostrada", "0715625489", "email@dom.ru");
        repC.addContact(contact);
    }
}
