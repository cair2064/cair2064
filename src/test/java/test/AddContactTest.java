package test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class AddContactTest {

	private Contact con;
	private RepositoryContact rep;
	private static final String INVALID_NAME = "Invalid name";
	private static final String INVALID_PHONE_NUMBER = "Invalid phone number";


	@Rule
	public ExpectedException expectedEx = ExpectedException.none();

	@Before
	public void setUp() throws Exception {
		rep = new RepositoryContactMock();
	}
	
	@Test
	public void testCase1()
	{
		try {
			con = new Contact("name", "address1", "+40711223344", "email1@domain1.com");
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		//int n = rep.count();
		rep.addContact(con);
		for(Contact c : rep.getContacts())
			if (c.equals(con))
			{
				assertTrue(true);
				break;
			}
		//assertTrue(n+1 == rep.count());
	}
	
	@Test
	public void testCase2()
	{
		try{
			rep.addContact((Contact) new Object());
		}
		catch(Exception e)
		{
			assertTrue(true);
		}	
	}
	
	@Test
	public void testCase3()
	{
		for(Contact c : rep.getContacts())
			rep.removeContact(c);

		try {
			con = new Contact("name", "address1", "+0711223344", "email1@domain1.com");
			rep.addContact(con);
		} catch (InvalidFormatException e) {
			assertTrue(false);
		}
		int n  = rep.count();
		if (n == 1)
			if (con.equals(rep.getContacts().get(0))) assertTrue(true);
			else assertTrue(false);
		else assertTrue(false);
	}

	/*
	Record added
	 */
	@Test
	public void test1() throws InvalidFormatException {
		Contact contact = new Contact("Alex", "Str. Soarelui", "0715625489", "email@domain.ro");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	/*
	Phone length < 10
	 */
	@Test
	public void test2() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_PHONE_NUMBER);
		Contact contact = new Contact("Cineva", "Str. Marin Preda, nr. 15", "071562548", "email@domain.com");
		rep.addContact(contact);
	}

	/*
	Phone length > 12
	 */
	@Test
	public void test3() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_PHONE_NUMBER);
		Contact contact = new Contact("Altcineva", "Str. Piezisa", "++40715625489", "email@domain.ro");
		rep.addContact(contact);
	}

	/*
	Name length < 3
	 */
	@Test
	public void test4() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_NAME);
		Contact contact = new Contact("Da", "Str. Marin Preda, nr. 15", "0715625489", "email@dom.com");
		rep.addContact(contact);
	}

	/*
	Name length > 30
	 */
	@Test
	public void test5() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_NAME);
		Contact contact = new Contact("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "Str. Autostrada", "0715625489", "email@dom.ru");
		rep.addContact(contact);
	}

	/*
	Name length < 3
	 */
	@Test
	public void test6() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_NAME);
		Contact contact = new Contact("", "Str. Soarelui", "0745645511", "email@dom.com");
		rep.addContact(contact);
	}

	/*
	Record added
	 */
	@Test
	public void test7() throws InvalidFormatException {
		Contact contact = new Contact("Ana", "Str. Soarelui", "0715625489", "email@domain.ro");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	/*
	Record added
	 */
	@Test
	public void test8() throws InvalidFormatException {
		Contact contact = new Contact("AAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "Str. Soarelui", "0745645511", "email@dom.com");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	/*
	Record added
	 */
	@Test
	public void test9() throws InvalidFormatException {
		Contact contact = new Contact("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", "Str. Soarelui", "0745645511", "email@dom.com");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	/*
	Phone length < 10
	 */
	@Test
	public void test10() throws InvalidFormatException {
		expectedEx.expect(InvalidFormatException.class);
		expectedEx.expectMessage(INVALID_PHONE_NUMBER);
		Contact contact = new Contact("Ana", "Str. Lunga", "", "email@dom.ru");
		rep.addContact(contact);
	}

	/*
	Record added
	 */
	@Test
	public void test11() throws InvalidFormatException {
		Contact contact = new Contact("Alex", "Str. Lunga", "+40754642132", "email@dom.ru");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}

	/*
	Record added
	 */
	@Test
	public void test12() throws InvalidFormatException {
		Contact contact = new Contact("Alex", "Str. Lunga", "+0754642132", "email@dom.ru");
		rep.addContact(contact);
		assertEquals(4, rep.getContacts().size());
	}
}