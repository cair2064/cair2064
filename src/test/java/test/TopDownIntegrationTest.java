package test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityMock;
import agenda.model.repository.classes.RepositoryContactMock;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

public class TopDownIntegrationTest {
    private RepositoryContact repC;
    private RepositoryActivity repA;


    @Before
    public void setUp(){
        repC = new RepositoryContactMock();
        repA = new RepositoryActivityMock();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    /*
    Test integrare pachet A - valid
     */
    @Test
    public void testIntegrareA() throws InvalidFormatException {
        Contact contact = new Contact("Alex", "Str. Soarelui", "0715625489", "email@domain.ro");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());
        assertEquals(repC.getContacts().get(3).getName(),"Alex");
        assertEquals(repC.getContacts().get(3).getAddress(),"Str. Soarelui");
    }

    /*
    Test de integrare |P->B->A| cu A-valid, B-invalid
    */
    @Test
    public void testIntegrareAB() throws Exception {
        //B
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("name1",
                df.parse("02/02/2010 12:00"),
                df.parse("01/01/2010 12:00"),
                null,
                "Lunch breakk");

        repA.addActivity(activity);
        assertFalse(repA.addActivity(activity));
        assertEquals(0, repA.getActivities().size());
        //A
        Contact contact = new Contact("Ion", "address1", "072627282833","email@domain.ro");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());
    }

    /*
    Test de integrare |P->B->A->C| cu B-invalid, A-valid, C-valid
    */
    @Test
    public void testIntegrareABC() throws Exception {
        //B
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("name1",
                df.parse("02/02/2010 12:00"),
                df.parse("01/01/2010 12:00"),
                null,
                "Lunch breakk");

        repA.addActivity(activity);
        assertFalse(repA.addActivity(activity));
        assertEquals(0, repA.getActivities().size());

        //A
        Contact contact = new Contact("Ionut", "address1", "072627282833","email@domain.ro");
        repC.addContact(contact);
        assertEquals(4, repC.getContacts().size());

        //C
        LinkedList<Contact> linkedlist = new LinkedList<Contact>();
        linkedlist.add(contact);
        for (Activity a : repA.getActivities())
            repA.removeActivity(a);

        Calendar c = Calendar.getInstance();
        c.set(2013, 3 - 1, 20, 12, 00);
        Date start = c.getTime();

        c.set(2013, 3 - 1, 20, 12, 30);
        Date end = c.getTime();

        Activity act = new Activity("name2", start, end,
                linkedlist, "description2");

        repA.addActivity(act);

        c.set(2013, 3 - 1, 20);
        List<Activity> result = repA.activitiesOnDate(act.getName(), c.getTime());
        assertTrue(result.size() == 1);
    }
}
