package agenda.model.base;

import agenda.exceptions.InvalidFormatException;
import agenda.model.validator.Validator;

public class Contact {
	private String Name;
	private String Address;
	private String Telefon;
	private String Email;

	public Contact(){
		Name = "";
		Address = "";
		Telefon = "";
		Email = "";
	}
	
	public Contact(String name, String address, String telefon, String email) throws InvalidFormatException{
		if (!Validator.validTelefon(telefon)) throw new InvalidFormatException("Invalid phone number");
		if (!Validator.validName(name)) throw new InvalidFormatException("Invalid name");
		if (!Validator.validAddress(address)) throw new InvalidFormatException("Invalid address");
		if (!Validator.validEmail(email)) throw new InvalidFormatException("Invalid email");

		Name = name;
		Address = address;
		Telefon = telefon;
		Email = email;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) throws InvalidFormatException {
		if (!Validator.validName(name)) throw new InvalidFormatException("Invalid name");
		Name = name;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) throws InvalidFormatException {
		if (!Validator.validAddress(address)) throw new InvalidFormatException("Invalid address");
		Address = address;
	}

	public String getTelefon() {
		return Telefon;
	}

	public void setTelefon(String telefon) throws InvalidFormatException {
		if (!Validator.validTelefon(telefon)) throw new InvalidFormatException("Invalid phone number");
		Telefon = telefon;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) throws InvalidFormatException {
		if (!Validator.validEmail(email)) throw new InvalidFormatException("Invalid email");
		Email = email;
	}

	public static Contact fromString(String str, String delim) throws InvalidFormatException
	{
		String[] s = str.split(delim);
		if (s.length!=4) throw new InvalidFormatException("Invalid data");
		if (!Validator.validTelefon(s[2])) throw new InvalidFormatException("Invalid phone number");
		if (!Validator.validName(s[0])) throw new InvalidFormatException("Invalid name");
		if (!Validator.validAddress(s[1])) throw new InvalidFormatException("Invalid address");
		if (!Validator.validAddress(s[3])) throw new InvalidFormatException("Invalid email");

		return new Contact(s[0], s[1], s[2], s[3]);
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(Name);
		sb.append("#");
		sb.append(Address);
		sb.append("#");
		sb.append(Telefon);
		sb.append("#");
		sb.append(Email);
		sb.append("#");
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (! (obj instanceof Contact)) return false;
		Contact o = (Contact)obj;
		if (Name.equals(o.Name) && Address.equals(o.Address) &&
				Telefon.equals(o.Telefon) && Email.equals(o.Email))
			return true;
		return false;
	}
	
}
