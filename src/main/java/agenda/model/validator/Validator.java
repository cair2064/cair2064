package agenda.model.validator;

import java.util.regex.Pattern;

public class Validator {
    public static boolean validName(String str)
    {
        String[] s = str.split("[\\p{Punct}\\s]+");
        if (s.length>2) return false;
        if (str.length() < 3 || str.length() > 30) return false;
        return true;
    }

    public static boolean validAddress(String str)
    {
        return true;
    }

    public static boolean validTelefon(String tel)
    {
        String[] s = tel.split("[\\p{Punct}\\s]+");
        if (tel.length() < 10 || tel.length() > 12) return false;
        if (tel.charAt(0) == '+' && s.length == 2 ) return true;
        if (tel.charAt(0) != '0')return false;
        if (s.length != 1) return false;
        return true;
    }


    public static boolean validEmail(String email)
    {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\."+
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }
}
